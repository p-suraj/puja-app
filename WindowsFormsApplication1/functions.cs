﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class Functions
    {
        public SqlConnection _connection;
        public SqlCommand _command;

        public Functions()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString);
        }
        public Boolean dml(string qry)
        {
            try
            {
                conOpen();
                SqlCommand _command = new SqlCommand(qry, _connection);
                _command.ExecuteNonQuery();
                conClose();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void conOpen()
        {
            try
            {
                if (_connection.State == ConnectionState.Closed)
                    _connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex, "Exception");
            }
        }
        public void conClose()
        {
            try
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex, "Exception");
            }
        }

        public void Fillgrid(DataGridView dg, string qry)
        {
            try
            {
                conOpen();
                SqlDataAdapter da = new SqlDataAdapter(qry, _connection);
                DataSet ds = new DataSet();
                da.Fill(ds, "Pujari");
                dg.DataSource = ds.Tables["Pujari"];
                dg.Refresh();
                conClose();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }
    }
}
