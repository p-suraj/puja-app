﻿using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource6 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PaymentDs = new WindowsFormsApplication1.PaymentDs();
            this.gv_payment = new System.Windows.Forms.DataGridView();
            this.gp_payment = new System.Windows.Forms.GroupBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.lblpayment = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_msg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lbltotamt = new System.Windows.Forms.Label();
            this.lblbal = new System.Windows.Forms.Label();
            this.lbl_bal = new System.Windows.Forms.Label();
            this.btn_paid = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Payment = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_totamt = new System.Windows.Forms.Label();
            this.lbl_quantity = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.gvDetails = new System.Windows.Forms.DataGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.GVpaymentDetails = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblsearch = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataTable1TableAdapter = new WindowsFormsApplication1.PaymentDsTableAdapters.DataTable1TableAdapter();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_payment)).BeginInit();
            this.gp_payment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVpaymentDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.PaymentDs;
            // 
            // PaymentDs
            // 
            this.PaymentDs.DataSetName = "PaymentDs";
            this.PaymentDs.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gv_payment
            // 
            this.gv_payment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_payment.Location = new System.Drawing.Point(23, 53);
            this.gv_payment.Name = "gv_payment";
            this.gv_payment.Size = new System.Drawing.Size(168, 467);
            this.gv_payment.TabIndex = 0;
            this.gv_payment.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_payment_RowHeaderMouseDoubleClick);
            // 
            // gp_payment
            // 
            this.gp_payment.Controls.Add(this.label5);
            this.gp_payment.Controls.Add(this.textBox2);
            this.gp_payment.Controls.Add(this.btnPrint);
            this.gp_payment.Controls.Add(this.btn_cancel);
            this.gp_payment.Controls.Add(this.lblpayment);
            this.gp_payment.Controls.Add(this.label3);
            this.gp_payment.Controls.Add(this.lbl_msg);
            this.gp_payment.Controls.Add(this.label1);
            this.gp_payment.Controls.Add(this.dateTimePicker1);
            this.gp_payment.Controls.Add(this.lbltotamt);
            this.gp_payment.Controls.Add(this.lblbal);
            this.gp_payment.Controls.Add(this.lbl_bal);
            this.gp_payment.Controls.Add(this.btn_paid);
            this.gp_payment.Controls.Add(this.label4);
            this.gp_payment.Controls.Add(this.txt_Payment);
            this.gp_payment.Controls.Add(this.label2);
            this.gp_payment.Controls.Add(this.lbl_date);
            this.gp_payment.Controls.Add(this.lbl_totamt);
            this.gp_payment.Controls.Add(this.lbl_quantity);
            this.gp_payment.Controls.Add(this.lbl_name);
            this.gp_payment.Location = new System.Drawing.Point(932, 24);
            this.gp_payment.Name = "gp_payment";
            this.gp_payment.Size = new System.Drawing.Size(375, 496);
            this.gp_payment.TabIndex = 1;
            this.gp_payment.TabStop = false;
            this.gp_payment.Text = "Payment";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(176, 414);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(78, 27);
            this.btnPrint.TabIndex = 19;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(263, 414);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(76, 27);
            this.btn_cancel.TabIndex = 18;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // lblpayment
            // 
            this.lblpayment.AutoSize = true;
            this.lblpayment.Location = new System.Drawing.Point(287, 111);
            this.lblpayment.Name = "lblpayment";
            this.lblpayment.Size = new System.Drawing.Size(0, 13);
            this.lblpayment.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Payment";
            // 
            // lbl_msg
            // 
            this.lbl_msg.AutoSize = true;
            this.lbl_msg.Location = new System.Drawing.Point(39, 361);
            this.lbl_msg.Name = "lbl_msg";
            this.lbl_msg.Size = new System.Drawing.Size(0, 13);
            this.lbl_msg.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 313);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(135, 306);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 13;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // lbltotamt
            // 
            this.lbltotamt.AutoSize = true;
            this.lbltotamt.Location = new System.Drawing.Point(81, 110);
            this.lbltotamt.Name = "lbltotamt";
            this.lbltotamt.Size = new System.Drawing.Size(0, 13);
            this.lbltotamt.TabIndex = 12;
            // 
            // lblbal
            // 
            this.lblbal.AutoSize = true;
            this.lblbal.Location = new System.Drawing.Point(184, 111);
            this.lblbal.Name = "lblbal";
            this.lblbal.Size = new System.Drawing.Size(0, 13);
            this.lblbal.TabIndex = 11;
            // 
            // lbl_bal
            // 
            this.lbl_bal.AutoSize = true;
            this.lbl_bal.Location = new System.Drawing.Point(180, 84);
            this.lbl_bal.Name = "lbl_bal";
            this.lbl_bal.Size = new System.Drawing.Size(46, 13);
            this.lbl_bal.TabIndex = 10;
            this.lbl_bal.Text = "Balance";
            // 
            // btn_paid
            // 
            this.btn_paid.Location = new System.Drawing.Point(95, 414);
            this.btn_paid.Name = "btn_paid";
            this.btn_paid.Size = new System.Drawing.Size(72, 27);
            this.btn_paid.TabIndex = 9;
            this.btn_paid.Text = "Paid";
            this.btn_paid.UseVisualStyleBackColor = true;
            this.btn_paid.Click += new System.EventHandler(this.btn_paid_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Payment";
            // 
            // txt_Payment
            // 
            this.txt_Payment.Location = new System.Drawing.Point(135, 247);
            this.txt_Payment.Name = "txt_Payment";
            this.txt_Payment.Size = new System.Drawing.Size(133, 20);
            this.txt_Payment.TabIndex = 7;
            this.txt_Payment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Payment_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "TotalAmount";
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(322, 111);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(0, 13);
            this.lbl_date.TabIndex = 3;
            // 
            // lbl_totamt
            // 
            this.lbl_totamt.AutoSize = true;
            this.lbl_totamt.Location = new System.Drawing.Point(177, 111);
            this.lbl_totamt.Name = "lbl_totamt";
            this.lbl_totamt.Size = new System.Drawing.Size(0, 13);
            this.lbl_totamt.TabIndex = 2;
            // 
            // lbl_quantity
            // 
            this.lbl_quantity.AutoSize = true;
            this.lbl_quantity.Location = new System.Drawing.Point(48, 111);
            this.lbl_quantity.Name = "lbl_quantity";
            this.lbl_quantity.Size = new System.Drawing.Size(0, 13);
            this.lbl_quantity.TabIndex = 1;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(45, 43);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(0, 13);
            this.lbl_name.TabIndex = 0;
            // 
            // gvDetails
            // 
            this.gvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvDetails.Location = new System.Drawing.Point(256, 24);
            this.gvDetails.Name = "gvDetails";
            this.gvDetails.Size = new System.Drawing.Size(645, 267);
            this.gvDetails.TabIndex = 2;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // GVpaymentDetails
            // 
            this.GVpaymentDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVpaymentDetails.Location = new System.Drawing.Point(256, 319);
            this.GVpaymentDetails.Name = "GVpaymentDetails";
            this.GVpaymentDetails.Size = new System.Drawing.Size(244, 201);
            this.GVpaymentDetails.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(67, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(133, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblsearch
            // 
            this.lblsearch.AutoSize = true;
            this.lblsearch.Location = new System.Drawing.Point(20, 24);
            this.lblsearch.Name = "lblsearch";
            this.lblsearch.Size = new System.Drawing.Size(41, 13);
            this.lblsearch.TabIndex = 5;
            this.lblsearch.Text = "Search";
            // 
            // reportViewer1
            // 
            reportDataSource6.Name = "DataSet1";
            reportDataSource6.Value = this.DataTable1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource6);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.PaymentReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(530, 319);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(371, 257);
            this.reportViewer1.TabIndex = 6;
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(126, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Name";
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 690);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.lblsearch);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.GVpaymentDetails);
            this.Controls.Add(this.gvDetails);
            this.Controls.Add(this.gp_payment);
            this.Controls.Add(this.gv_payment);
            this.Name = "Payment";
            this.Text = "Payment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Payment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_payment)).EndInit();
            this.gp_payment.ResumeLayout(false);
            this.gp_payment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVpaymentDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gv_payment;
        private System.Windows.Forms.GroupBox gp_payment;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_totamt;
        private System.Windows.Forms.Label lbl_quantity;
        private System.Windows.Forms.Button btn_paid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Payment;
        private System.Windows.Forms.Label lbl_bal;
        private System.Windows.Forms.Label lblbal;
        private System.Windows.Forms.Label lbltotamt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lbl_msg;
        private System.Windows.Forms.DataGridView gvDetails;
        private System.Windows.Forms.Label lblpayment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.DataGridView GVpaymentDetails;
        private System.Windows.Forms.Label lblsearch;
        private System.Windows.Forms.TextBox textBox1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private PaymentDs PaymentDs;
        private PaymentDsTableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;

        
    }
}