﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class PujaOrder : Form
    {
        //DataTable data = new DataTable();
        private Functions f;
        private SqlCommand _command1 = new SqlCommand();

        public PujaOrder()
        {
            InitializeComponent();
            f = new Functions();
            f._command = new SqlCommand();
            FillCombo();
            //FillSubitem();
        }

        private void FillCombo()
        {
            f._command.Connection = f._connection;
            f._command.CommandType = CommandType.Text;
            f._command.CommandText = "SELECT item_id,item_name FROM Item";
            //DataSet objDs = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = f._command;
            f.conOpen();
            dAdapter.Fill(dt);
            f.conClose();
            ddl_item.ValueMember = "item_id";
            ddl_item.DisplayMember = "item_name";
            //ddl_item.DataSource = objDs.Tables[0];

            ddl_item.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["item_name"] = "--Select--";
            dr["item_id"] = 0;

            dt.Rows.InsertAt(dr, 0);
            ddl_item.SelectedIndex = 0;
        }

        private void FillSubitem(int itemId)
        {
            f._command.Connection = f._connection;
            f._command.CommandType = CommandType.Text;
            f._command.CommandText = "SELECT s_id, subitem_name FROM subItem WHERE item_id=" + itemId + "";
            //DataSet objDs = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = f._command;
            f.conOpen();
            dAdapter.Fill(dt);
            f.conClose();
            if (dt.Rows.Count > 0)
            {
                ddl_subitem.ValueMember = "s_id";
                ddl_subitem.DisplayMember = "subitem_name";
                ddl_subitem.DataSource = dt;

                DataRow dr = dt.NewRow();
                dr["subitem_name"] = "--Select--";
                dr["s_id"] = 0;

                dt.Rows.InsertAt(dr, 0);
                ddl_subitem.SelectedIndex = 0;
            }
            else
            {
                ddl_subitem.DisplayMember = " ";
            }

        }

        private void LoadGrid()
        {
            try
            {
                f.conOpen();
                var query = "select pid,p_name as PujariName from Pujari";
                f.Fillgrid(gv_pujari, query);
                this.gv_pujari.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Clear()
        {
            try
            {
                txt_amt.Text = "";
                txt_count.Text = "0";
                ddl_item.SelectedIndex = 0;
                ddl_subitem.SelectedIndex = 0;
                txt_payment.Text = "0";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

        private void PujaOrder_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet2.DataTable1' table. You can move, or remove it, as needed.
            //this.DataTable1TableAdapter.Fill(this.dataSet2.DataTable1);
            // TODO: This line of code loads data into the 'DataSet1.puja_order' table. You can move, or remove it, as needed.
            //this.puja_orderTableAdapter1.Fill(this.DataSet2.puja_order);
            //this.DataTable1TableAdapter.Fill(this.dataSet2.DataTable1);
            LoadGrid();
            gporder.Hide();

            this.reportViewer1.RefreshReport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gv_pujari_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            gporder.Show();
            try
            {
                f.conOpen();
                var row = gv_pujari.SelectedCells[0].Value;
                var getDetails = "select p_name,mob1,address from Pujari where pid=" + row + "";
                var adp = new SqlDataAdapter(getDetails, f._connection);
                var dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                    label2.Text = dt.Rows[0][0].ToString();
                label6.Text = dt.Rows[0][1].ToString();
                label7.Text = dt.Rows[0][2].ToString();

                f.conClose();
            }
            catch (Exception ex)
            {

            }
        }

        private void ddl_item_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_item.SelectedIndex >= 0)
            {
                FillSubitem((int)ddl_item.SelectedValue);
            }
        }

        private void Order_Click(object sender, EventArgs e)
        {

            string value = "";
            bool isChecked = rb_cash.Checked;
            if (isChecked)
                value = rb_cash.Text;
            else
                value = rb_udhar.Text;
            try
            {
                if (ddl_item.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select Item", "Requiered");
                    ddl_item.Focus();
                }
                else if (ddl_subitem.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select SubItem", "Requiered");
                    ddl_subitem.Focus();
                }
                else if (!txtEmpty(txt_count.Text))
                {
                    MessageBox.Show("Please Enter Quantity", "Requiered");
                    txt_count.Focus();
                }
                else if (!txtEmpty(txt_amt.Text))
                {
                    MessageBox.Show("Please Enter Amount", "Requiered");
                    txt_amt.Focus();
                }
                //else if (!txtEmpty(txt_payment.Text))
                //{
                //    MessageBox.Show("Please Enter Zero Payment", "Requiered");
                //    txt_payment.Focus();
                //}

                else
                {
                    
                    f.conOpen();
                    var placeOrder = "insert into puja_order(pid,item_id,subitem_id,quantity,TotalAmount,Date,paytype) values("
                                     + gv_pujari.SelectedCells[0].Value + "," + ddl_item.SelectedValue + "," +
                                     ddl_subitem.SelectedValue + ",'" + txt_count.Text + "'," +
                                     txt_amt.Text + ",'" + dateTimePicker1.Value.ToString("yyyy-MM-ddTHH:MM:ss") + "','" + value + "')";

                    if (value=="Udhar")
                    {
                        txt_payment.Text = "0";
                    }
                    else
                    {
                        txt_payment.Text=txt_payment.Text;
                    }
                    var addpayment = "insert into Payment (pid,TotalAmount,date,Payment) values" + "(" +
                                     gv_pujari.SelectedCells[0].Value + "," + txt_amt.Text + "," +
                                     "'" + dateTimePicker1.Value.ToString("yyyy-MM-ddTHH:MM:ss") + "'," + txt_payment.Text + ")";
                    f.dml(placeOrder);
                    f.dml(addpayment);
                    f.conClose();

                    MessageBox.Show("Order Placed Successfully...");
                    Clear();
                }
                this.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ddl_subitem_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddl_subitem.SelectedIndex > 0)
            //{
            //    int subitemId = Convert.ToInt32(ddl_subitem.SelectedValue);
            //}
        }

        private void txt_count_TextChanged(object sender, EventArgs e)
        {
            try
            {
                f._command.CommandType = CommandType.Text;
                f._command.CommandText = "select amount from subItem where s_id=" + ddl_subitem.SelectedValue + "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlDataReader dr;
                da.SelectCommand = f._command;
                f.conOpen();
                da.Fill(ds);
                f.conClose();
                var amt = ds.Tables[0].Rows[0][0].ToString();
                txt_amt.Text = (Convert.ToInt32(txt_count.Text) * Convert.ToInt32(amt)).ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = gv_pujari.DataSource;
            bs.Filter = "PujariName" + " like '%" + textBox1.Text + "%'";
            gv_pujari.DataSource = bs;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           f.conOpen();
           this.DataTable1TableAdapter.Fill(this.dataSet2.DataTable1);
            this.reportViewer1.RefreshReport();
            f.conClose();

        }
        public bool txtEmpty(String txtbox)
        {
            string name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }

        private void rb_cash_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_cash.Text == "Cash")
            {
                txt_payment.Visible = true;
            }
            else
            {
                txt_payment.Visible = false;
            }
        }

        private void rb_udhar_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_udhar.Text == "Udhar")
            {
                txt_payment.Visible = false;
            }
        }

        private void gporder_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today.Date;
            if (dateTimePicker1.Value < today)
            {
                MessageBox.Show("You are not allowed to select older day than today!","Warning");
                dateTimePicker1.Value = DateTime.Today.Date;
            }
        }

    }
}
