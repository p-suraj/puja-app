﻿namespace WindowsFormsApplication1
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource6 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.ReportViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PaymentReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PaymentDetailsReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Gvpujari = new System.Windows.Forms.DataGridView();
            this.DataTable1TableAdapter = new WindowsFormsApplication1.DataSet2TableAdapters.DataTable1TableAdapter();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ReportdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerto = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PaymentDetailsreportViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ExpenseDS = new WindowsFormsApplication1.ExpenseDS();
            this.dTTableAdapter = new WindowsFormsApplication1.ExpenseDSTableAdapters.DTTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ReportViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDetailsReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gvpujari)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseDS)).BeginInit();
            this.SuspendLayout();
            // 
            // Gvpujari
            // 
            this.Gvpujari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Gvpujari.Location = new System.Drawing.Point(13, 70);
            this.Gvpujari.Name = "Gvpujari";
            this.Gvpujari.Size = new System.Drawing.Size(163, 246);
            this.Gvpujari.TabIndex = 0;
            this.Gvpujari.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Gvpujari_RowHeaderMouseDoubleClick);
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer1
            // 
            reportDataSource4.Name = "DataSet1";
            reportDataSource4.Value = this.ReportViewBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.Report2.rdlc";
            this.reportViewer1.LocalReport.ReportPath = "Report2.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(200, 70);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(538, 246);
            this.reportViewer1.TabIndex = 1;
            // 
            // reportViewer2
            // 
            reportDataSource5.Name = "DataSet1";
            reportDataSource5.Value = this.PaymentReportBindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource5);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.ReportpaymentView.rdlc";
            this.reportViewer2.LocalReport.ReportPath = "ReportpaymentView.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(200, 332);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(311, 307);
            this.reportViewer2.TabIndex = 2;
            // 
            // ReportdateTimePicker
            // 
            this.ReportdateTimePicker.Location = new System.Drawing.Point(45, 30);
            this.ReportdateTimePicker.Name = "ReportdateTimePicker";
            this.ReportdateTimePicker.Size = new System.Drawing.Size(131, 20);
            this.ReportdateTimePicker.TabIndex = 3;
            // 
            // dateTimePickerto
            // 
            this.dateTimePickerto.Location = new System.Drawing.Point(222, 30);
            this.dateTimePickerto.Name = "dateTimePickerto";
            this.dateTimePickerto.Size = new System.Drawing.Size(136, 20);
            this.dateTimePickerto.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "To";
            // 
            // PaymentDetailsreportViewer
            // 
            reportDataSource6.Name = "DataSet1";
            reportDataSource6.Value = this.PaymentDetailsReportBindingSource;
            this.PaymentDetailsreportViewer.LocalReport.DataSources.Add(reportDataSource6);
            this.PaymentDetailsreportViewer.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.PaymentDetailsReport.rdlc";
            this.PaymentDetailsreportViewer.LocalReport.ReportPath = "PaymentDetailsReport.rdlc";
            this.PaymentDetailsreportViewer.Location = new System.Drawing.Point(532, 332);
            this.PaymentDetailsreportViewer.Name = "PaymentDetailsreportViewer";
            this.PaymentDetailsreportViewer.Size = new System.Drawing.Size(286, 307);
            this.PaymentDetailsreportViewer.TabIndex = 7;
            // 
            // ExpenseDS
            // 
            this.ExpenseDS.DataSetName = "ExpenseDS";
            this.ExpenseDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dTTableAdapter
            // 
            this.dTTableAdapter.ClearBeforeFill = true;
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 678);
            this.Controls.Add(this.PaymentDetailsreportViewer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePickerto);
            this.Controls.Add(this.ReportdateTimePicker);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.Gvpujari);
            this.Name = "Report";
            this.Text = "Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Report_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ReportViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDetailsReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gvpujari)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseDS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Gvpujari;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private DataSet2TableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource ReportViewBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.BindingSource PaymentReportBindingSource;
        private System.Windows.Forms.DateTimePicker ReportdateTimePicker;
        private System.Windows.Forms.DateTimePicker dateTimePickerto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Microsoft.Reporting.WinForms.ReportViewer PaymentDetailsreportViewer;
        private System.Windows.Forms.BindingSource PaymentDetailsReportBindingSource;
        private ExpenseDS ExpenseDS;
        private ExpenseDSTableAdapters.DTTableAdapter dTTableAdapter;
        
    }
}