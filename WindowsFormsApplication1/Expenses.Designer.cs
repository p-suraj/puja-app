﻿namespace WindowsFormsApplication1
{
    partial class Expenses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.dTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ExpenseDS = new WindowsFormsApplication1.ExpenseDS();
            this.txt_comment = new System.Windows.Forms.TextBox();
            this.txt_amt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_expense = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.txtExpense = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAddExpense = new System.Windows.Forms.Button();
            this.ddl_ExpenseType = new System.Windows.Forms.ComboBox();
            this.GVExpense = new System.Windows.Forms.DataGridView();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dTTableAdapter = new WindowsFormsApplication1.ExpenseDSTableAdapters.DTTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVExpense)).BeginInit();
            this.SuspendLayout();
            // 
            // dTBindingSource
            // 
            this.dTBindingSource.DataMember = "DT";
            this.dTBindingSource.DataSource = this.ExpenseDS;
            // 
            // ExpenseDS
            // 
            this.ExpenseDS.DataSetName = "ExpenseDS";
            this.ExpenseDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txt_comment
            // 
            this.txt_comment.Location = new System.Drawing.Point(456, 102);
            this.txt_comment.Multiline = true;
            this.txt_comment.Name = "txt_comment";
            this.txt_comment.Size = new System.Drawing.Size(155, 33);
            this.txt_comment.TabIndex = 1;
            // 
            // txt_amt
            // 
            this.txt_amt.Location = new System.Drawing.Point(456, 155);
            this.txt_amt.Name = "txt_amt";
            this.txt_amt.Size = new System.Drawing.Size(155, 20);
            this.txt_amt.TabIndex = 2;
            this.txt_amt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amt_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(340, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Expense Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Comment";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(340, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Amount";
            // 
            // btn_expense
            // 
            this.btn_expense.Location = new System.Drawing.Point(456, 218);
            this.btn_expense.Name = "btn_expense";
            this.btn_expense.Size = new System.Drawing.Size(78, 27);
            this.btn_expense.TabIndex = 6;
            this.btn_expense.Text = "Add";
            this.btn_expense.UseVisualStyleBackColor = true;
            this.btn_expense.Click += new System.EventHandler(this.btn_expense_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(554, 218);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(78, 27);
            this.btn_cancel.TabIndex = 7;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Location = new System.Drawing.Point(12, 177);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 13);
            this.lblmsg.TabIndex = 8;
            // 
            // txtExpense
            // 
            this.txtExpense.Location = new System.Drawing.Point(131, 67);
            this.txtExpense.Name = "txtExpense";
            this.txtExpense.Size = new System.Drawing.Size(155, 20);
            this.txtExpense.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Add Expense";
            // 
            // btnAddExpense
            // 
            this.btnAddExpense.Location = new System.Drawing.Point(131, 130);
            this.btnAddExpense.Name = "btnAddExpense";
            this.btnAddExpense.Size = new System.Drawing.Size(78, 27);
            this.btnAddExpense.TabIndex = 11;
            this.btnAddExpense.Text = "Add";
            this.btnAddExpense.UseVisualStyleBackColor = true;
            this.btnAddExpense.Click += new System.EventHandler(this.btnAddExpense_Click);
            // 
            // ddl_ExpenseType
            // 
            this.ddl_ExpenseType.FormattingEnabled = true;
            this.ddl_ExpenseType.Location = new System.Drawing.Point(456, 58);
            this.ddl_ExpenseType.Name = "ddl_ExpenseType";
            this.ddl_ExpenseType.Size = new System.Drawing.Size(155, 21);
            this.ddl_ExpenseType.TabIndex = 12;
            // 
            // GVExpense
            // 
            this.GVExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVExpense.Location = new System.Drawing.Point(15, 292);
            this.GVExpense.Name = "GVExpense";
            this.GVExpense.Size = new System.Drawing.Size(329, 164);
            this.GVExpense.TabIndex = 13;
            // 
            // reportViewer1
            // 
            reportDataSource2.Name = "DataSet1";
            reportDataSource2.Value = this.dTBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.ExpenseReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(385, 292);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(396, 246);
            this.reportViewer1.TabIndex = 14;
            // 
            // dTTableAdapter
            // 
            this.dTTableAdapter.ClearBeforeFill = true;
            // 
            // Expenses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1295, 550);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.GVExpense);
            this.Controls.Add(this.ddl_ExpenseType);
            this.Controls.Add(this.btnAddExpense);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtExpense);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_expense);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_amt);
            this.Controls.Add(this.txt_comment);
            this.Name = "Expenses";
            this.Text = "Expenses";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Expenses_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpenseDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVExpense)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_comment;
        private System.Windows.Forms.TextBox txt_amt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_expense;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Button btnAddExpense;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtExpense;
        private System.Windows.Forms.ComboBox ddl_ExpenseType;
        private System.Windows.Forms.DataGridView GVExpense;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private ExpenseDS ExpenseDS;
        private System.Windows.Forms.BindingSource dTBindingSource;
        private ExpenseDSTableAdapters.DTTableAdapter dTTableAdapter;
    }
}