﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Payment : Form
    {
        private Functions f;

        public Payment()
        {
            InitializeComponent();
            f = new Functions();
            f._command = new SqlCommand();
            LoadGrid();
            //LoadDetailsGrid();
        }

        private void LoadGrid()
        {
            try
            {
                var query = "select pid,p_name as PujariName from Pujari";
                f.Fillgrid(gv_payment, query);
                this.gv_payment.Columns[0].Visible = false;
            }
            catch (Exception ex) { }
        }

        private void LoadDetailsGrid()
        {
            try
            {
                var row = gv_payment.SelectedCells[0].Value;
                var query = "select I.item_name as ItemName,SI.subItem_name as SubitemName,po.TotalAmount,po.paytype as Paytype,po.quantity as Quantity,po.Date" +
                            " from Item I INNER JOIN puja_order po ON I.item_id=po.item_id " +
                            "INNER JOIN subItem SI ON SI.s_id=po.subitem_id where pid=" + row + "";
                f.Fillgrid(gvDetails, query);
                this.gvDetails.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadPaymentGrid()
        {
            try
            {
                var row = gv_payment.SelectedCells[0].Value;
                var query = "select Payment,Date from Payment where pid=" + row + "";
                f.Fillgrid(GVpaymentDetails, query);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gv_payment_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //gporder.Show();
            try
            {

                var row = gv_payment.SelectedCells[0].Value;
                f._command.CommandType = CommandType.Text;
                f._command.CommandText = "select SUM(TotalAmount)-SUM(Payment) as Balance,SUM(TotalAmount) as TotalAmount,SUM(Payment) as Payment  from Payment where pid=" + row + "";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlDataReader dr;
                da.SelectCommand = f._command;
                da.SelectCommand.Connection = f._connection;
                f.conOpen();
                da.Fill(ds);
                lblbal.Text = ds.Tables[0].Rows[0][0].ToString();
                lbltotamt.Text = ds.Tables[0].Rows[0][1].ToString();
                lblpayment.Text = ds.Tables[0].Rows[0][2].ToString();
                var getDetails = "select pid,p_name from Pujari where pid=" + row + "";
                var adp = new SqlDataAdapter(getDetails, f._connection);
                adp.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                    textBox2.Text = ds.Tables[0].Rows[1][4].ToString();
                f.conClose();
                LoadDetailsGrid();
                LoadPaymentGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_paid_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtEmpty(txt_Payment.Text))
                {
                    MessageBox.Show("Please Enter Payment Amount", "Required");
                    txt_Payment.Focus();
                }
                else
                {
                    f.conOpen();
                    var addpayment = "insert into Payment(pid,TotalAmount,Date,Payment) values(" +
                                     gv_payment.SelectedCells[0].Value + "," + "0" + ",'" +
                                     dateTimePicker1.Value.ToString("yyyy-MM-ddTHH:MM:ss") + "'," + txt_Payment.Text + ")";
                    f.dml(addpayment);
                    f.conClose();
                    MessageBox.Show("Amount Paid...");
                    txt_Payment.Text = "";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txt_Payment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = gv_payment.DataSource;
            bs.Filter = "PujariName" + " like '%" + textBox1.Text + "%'";
            gv_payment.DataSource = bs;
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'PaymentDs.DataTable1' table. You can move, or remove it, as needed.
            //this.DataTable1TableAdapter.Fill(this.PaymentDs.DataTable1);
            // TODO: This line of code loads data into the 'PaymentDs.DataTable1' table. You can move, or remove it, as needed.
            //this.DataTable1TableAdapter.Fill(this.PaymentDs.DataTable1);

            this.reportViewer1.RefreshReport();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            this.DataTable1TableAdapter.Fill(this.PaymentDs.DataTable1);
            this.reportViewer1.RefreshReport();

        }

        private bool txtEmpty(String txtbox)
        {
            var name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            var today = DateTime.Today.Date;
            if (dateTimePicker1.Value < today)
            {
                MessageBox.Show("You are not allowed to select older day than today!","Warning");
                dateTimePicker1.Value = DateTime.Today.Date;
            }
        }
    }
}
