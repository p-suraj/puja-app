﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Reporting.WinForms;

namespace WindowsFormsApplication1
{
    public partial class Report : Form
    {
        private Functions f;

        public Report()
        {
            InitializeComponent();
            f = new Functions();
            f._command = new SqlCommand();
            //f._connection =new SqlConnection();
        }

        private void Loadgrid()
        {
            string query = "select pid, p_name As PujariName from Pujari";
            f.Fillgrid(Gvpujari, query);
            this.Gvpujari.Columns[0].Visible = false;
        }

        private void Report_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ExpenseDS.DataTable1' table. You can move, or remove it, as needed.
            //this.dTTableAdapter.Fill(this.ExpenseDS.DT);
            // TODO: This line of code loads data into the 'DataSet2.DataTable1' table. You can move, or remove it, as needed.
            //this.DataTable1TableAdapter.Fill(this.individualreport.DataTable1);
            Loadgrid();
            this.reportViewer1.RefreshReport();
            this.reportViewer2.RefreshReport();
            this.PaymentDetailsreportViewer.RefreshReport();
        }

        private void Gvpujari_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var datefrom = ReportdateTimePicker.Value.ToString("yyyy-MM-ddTHH:MM:ss");
            var dateto = dateTimePickerto.Value.ToString("yyyy-MM-ddTHH:MM:ss");
            var row = Gvpujari.SelectedCells[0].Value;
            string query = "select I.item_name as ItemName,SI.subItem_name as SubitemName,po.TotalAmount,po.paytype as Paytype,po.Date" +
                " from Item I INNER JOIN puja_order po ON I.item_id=po.item_id" +
                " INNER JOIN subItem SI ON SI.s_id=po.subitem_id where pid=" + row + "and Date Between'"+ datefrom +"' and '"+dateto+"'";
            ////f._command.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(query, f._connection);
            DataSet ds = new DataSet("Mydataset");
            ////DataTable dt = new DataTable();
            ////dt.TableName = "DataTable1";
            f.conOpen();
            da.Fill(ds);
            f.conClose();
            var listOfReportView = new List<ReportView>();
            listOfReportView = ds.Tables[0].AsEnumerable().Select(x => new ReportView
            {
                Date = x.Field<DateTime>("Date").ToShortDateString(),
                ItemName = x.Field<string>("ItemName"),
                Paytype = x.Field<string>("Paytype"),
                //Quantity = x.Field<int>("Quantity").ToString(),
                SubitemName = x.Field<string>("SubitemName"),
                TotalAmount = x.Field<decimal>("TotalAmount").ToString()
            }).ToList();

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.ReportEmbeddedResource = "Report2.rdlc";

            ReportDataSource dataset = new ReportDataSource("DataSet1", listOfReportView); // set the datasource
            reportViewer1.LocalReport.DataSources.Add(dataset);
            dataset.Value = listOfReportView;

            reportViewer1.LocalReport.Refresh();
            reportViewer1.RefreshReport();
            ////ReportDataSource RDS = new ReportDataSource("individualreport", ds.Tables[0]);
            //ReportViewer RV = new ReportViewer();
            //RV.Visible = true;
            //RV.ProcessingMode = ProcessingMode.Local;
            //LocalReport lc = RV.LocalReport;
            
            //ds.Tables[0].WriteXmlSchema(@"/individualreport.xsd");
            //reportViewer1.ProcessingMode = ProcessingMode.Local;
            //ReportDataSource source = new ReportDataSource("Mydataset", ds.Tables[0]);

            //this.reportViewer1.LocalReport.ReportEmbeddedResource = "individualreport.rdlc";
            //reportViewer1.LocalReport.DataSources.Clear();
            //reportViewer1.LocalReport.DataSources.Add(source);
            //// reportViewer1.DataBind();
            //lc.DataSources.Add(source);
            //RV.LocalReport.Refresh();
            //reportViewer1.LocalReport.Refresh();
            string paymentreport="select TotalAmount,Payment,Date from Payment where pid="+row+"";
            SqlDataAdapter daa = new SqlDataAdapter(paymentreport, f._connection);
            DataSet ds1 = new DataSet();
            f.conOpen();
            daa.Fill(ds1);
            f.conClose();
            var listOfPaymentReport = new List<PaymentReport>();
            listOfPaymentReport = ds1.Tables[0].AsEnumerable().Select(x => new PaymentReport
            {
                Date = x.Field<DateTime>("Date").ToShortDateString(),
                Payment = x.Field<int>("Payment").ToString(),
                TotalAmount = x.Field<decimal>("TotalAmount").ToString()
               
            }).ToList();

            reportViewer2.LocalReport.DataSources.Clear();
            reportViewer2.LocalReport.ReportEmbeddedResource = "ReportpaymentView.rdlc";

            ReportDataSource reportdataset = new ReportDataSource("DataSet1", listOfPaymentReport); // set the datasource
            reportViewer2.LocalReport.DataSources.Add(reportdataset);
            reportdataset.Value = listOfPaymentReport;

            reportViewer2.LocalReport.Refresh();
            reportViewer2.RefreshReport();

            //Payment Details
            string paymentedetailsreport = "select SUM(TotalAmount) as TotalAmount,SUM(Payment) as Payment,SUM(TotalAmount)-SUM(Payment) as Balance from Payment where pid=" + row + "";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(paymentedetailsreport, f._connection);
            DataSet dataSet = new DataSet();
            f.conOpen();
            dataAdapter.Fill(dataSet);
            f.conClose();
            var listOfPaymentdetailsReport = new List<PaymentDetailsReport>();
            listOfPaymentdetailsReport = dataSet.Tables[0].AsEnumerable().Select(x => new PaymentDetailsReport
            {
                Payment = x.Field<int>("Payment").ToString(),
                TotalAmount = x.Field<decimal>("TotalAmount").ToString(),
                Balance = x.Field<decimal>("Balance").ToString()

            }).ToList();

            PaymentDetailsreportViewer.LocalReport.DataSources.Clear();
            PaymentDetailsreportViewer.LocalReport.ReportEmbeddedResource = "PaymentDetailsReport.rdlc";

            ReportDataSource reportdata = new ReportDataSource("DataSet1", listOfPaymentdetailsReport); // set the datasource
            PaymentDetailsreportViewer.LocalReport.DataSources.Add(reportdata);
            reportdata.Value = listOfPaymentdetailsReport;

            PaymentDetailsreportViewer.LocalReport.Refresh();
            PaymentDetailsreportViewer.RefreshReport();
        }
    }
}
