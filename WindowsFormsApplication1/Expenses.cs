﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Expenses : Form
    {
        private Functions f;
        public Expenses()
        {
            InitializeComponent();
            f = new Functions();
            f._command = new SqlCommand();
        }

        private void LoadGrid()
        {
            string query = "select Expense,comment as Comment,amount as Amount from Expense INNER JOIN AddExpense ON AddExpense.Expense_id=Expense.expense_type";
            f.Fillgrid(GVExpense,query);
        }

        private void Fillcombo()
        {
            f._command.Connection = f._connection;
            f._command.CommandType = CommandType.Text;
            f._command.CommandText = "SELECT Expense_id,Expense FROM AddExpense";
            //DataSet objDs = new DataSet();
            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = f._command;
            f.conOpen();
            dAdapter.Fill(dt);
            f.conClose();
            ddl_ExpenseType.ValueMember = "Expense_id";
            ddl_ExpenseType.DisplayMember = "Expense";
            //ddl_item.DataSource = objDs.Tables[0];

            ddl_ExpenseType.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["Expense"] = "--Select--";
            dr["Expense_id"] = 0;

            dt.Rows.InsertAt(dr, 0);
            ddl_ExpenseType.SelectedIndex = 0;
        }
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void clear()
        {
            txt_amt.Text = "";
            txt_comment.Text = "";
            //txt_expensetype.Text = "";
        }
        private void btn_expense_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddl_ExpenseType.SelectedIndex==0)
                {
                    MessageBox.Show("Please Select Expense Type", "Requiered");
                    ddl_ExpenseType.Focus();
                }
                else if (!txtEmpty(txt_amt.Text))
                {
                    MessageBox.Show("Please Enter Expense Amount", "Requiered");
                    txt_amt.Focus();
                }
                else
                {
                    f.conOpen();
                    var addexpense = "Insert into Expense(expense_type,comment,amount) values(" + ddl_ExpenseType.SelectedValue +
                                     ",'" + txt_comment.Text + "'," + txt_amt.Text + ")";
                    f.dml(addexpense);
                    f.conClose();
                    MessageBox.Show("Record Inserted Successfully...");
                    clear();
                    LoadGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txt_amt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void btnAddExpense_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtEmpty(txtExpense.Text))
                {
                    MessageBox.Show("Please Enter Expense", "Requiered");
                    txtExpense.Focus();
                }
                else
                {
                    f.conOpen();
                    string query = "insert into AddExpense (Expense) values('" + txtExpense.Text.Trim() + "')";
                    f.dml(query);
                    f.conClose();
                    MessageBox.Show("Expense Added Successfully..");
                    txtExpense.Text = "";
                    Fillcombo();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Expenses_Load(object sender, EventArgs e)
        {
// TODO: This line of code loads data into the 'ExpenseDS.DT' table. You can move, or remove it, as needed.
this.dTTableAdapter.Fill(this.ExpenseDS.DT);
            Fillcombo();
            LoadGrid();
            this.reportViewer1.RefreshReport();
            this.dTTableAdapter.Fill(this.ExpenseDS.DT);
        }

        public bool txtEmpty(String txtbox)
        {
            string name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }
    }
}
