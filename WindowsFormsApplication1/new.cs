﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class @new : Form
    {
        public @new()
        {
            InitializeComponent();
        }

        private Itemform _f1;
        private Addsubtem _f2;
        private Addpujari _f3;
        private PujaOrder _f4;
        private Expenses _f5;
        private Report _f6;
        private Payment _f7;

        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f1 = new Itemform { MdiParent = this };
            _f1.Show();
        }

        private void addSubItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f2 = new Addsubtem { MdiParent = this };
            _f2.Show();

        }

        private void addPujariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f3 = new Addpujari { MdiParent = this };
            _f3.Show();
        }

        private void pujaOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f4=new PujaOrder(){MdiParent = this};
            _f4.Show();
        }

        private void expensesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f5 = new Expenses(){ MdiParent = this };
            _f5.Show();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f6 = new Report() {MdiParent = this};
            _f6.Show();
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _f7 = new Payment() { MdiParent = this };
            _f7.Show();
        }

        private void @new_Load(object sender, EventArgs e)
        {
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (ActiveMdiChild != null)
                ActiveMdiChild.Close();
        }
    }
}
