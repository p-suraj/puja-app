﻿namespace WindowsFormsApplication1
{
    partial class Addsubtem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_addsubitem = new System.Windows.Forms.Button();
            this.ddl_item = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_subitem = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.txt_amt = new System.Windows.Forms.TextBox();
            this.lblamt = new System.Windows.Forms.Label();
            this.lblmsg = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.GVSubitem = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSubitem)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_addsubitem
            // 
            this.btn_addsubitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_addsubitem.Location = new System.Drawing.Point(170, 221);
            this.btn_addsubitem.Name = "btn_addsubitem";
            this.btn_addsubitem.Size = new System.Drawing.Size(78, 27);
            this.btn_addsubitem.TabIndex = 0;
            this.btn_addsubitem.Text = "Add";
            this.btn_addsubitem.UseVisualStyleBackColor = true;
            this.btn_addsubitem.Click += new System.EventHandler(this.btn_addsubitem_Click);
            // 
            // ddl_item
            // 
            this.ddl_item.FormattingEnabled = true;
            this.ddl_item.Location = new System.Drawing.Point(177, 49);
            this.ddl_item.Name = "ddl_item";
            this.ddl_item.Size = new System.Drawing.Size(162, 21);
            this.ddl_item.TabIndex = 1;
            this.ddl_item.SelectedIndexChanged += new System.EventHandler(this.ddl_item_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Item";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(56, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Add Sub Item";
            // 
            // txt_subitem
            // 
            this.txt_subitem.Location = new System.Drawing.Point(177, 103);
            this.txt_subitem.Name = "txt_subitem";
            this.txt_subitem.Size = new System.Drawing.Size(162, 20);
            this.txt_subitem.TabIndex = 4;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(356, 221);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(78, 27);
            this.btn_cancel.TabIndex = 5;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // txt_amt
            // 
            this.txt_amt.Location = new System.Drawing.Point(177, 161);
            this.txt_amt.Name = "txt_amt";
            this.txt_amt.Size = new System.Drawing.Size(162, 20);
            this.txt_amt.TabIndex = 6;
            this.txt_amt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amt_KeyPress);
            // 
            // lblamt
            // 
            this.lblamt.AutoSize = true;
            this.lblamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblamt.Location = new System.Drawing.Point(56, 168);
            this.lblamt.Name = "lblamt";
            this.lblamt.Size = new System.Drawing.Size(43, 13);
            this.lblamt.TabIndex = 7;
            this.lblamt.Text = "Amount";
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Location = new System.Drawing.Point(341, 324);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 13);
            this.lblmsg.TabIndex = 8;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // GVSubitem
            // 
            this.GVSubitem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVSubitem.Location = new System.Drawing.Point(496, 49);
            this.GVSubitem.Name = "GVSubitem";
            this.GVSubitem.Size = new System.Drawing.Size(378, 199);
            this.GVSubitem.TabIndex = 9;
            this.GVSubitem.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GVSubitem_RowHeaderMouseDoubleClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(263, 221);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(78, 27);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Addsubtem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 695);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.GVSubitem);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.lblamt);
            this.Controls.Add(this.txt_amt);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.txt_subitem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddl_item);
            this.Controls.Add(this.btn_addsubitem);
            this.Name = "Addsubtem";
            this.Text = "Addsubtem";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Addsubtem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSubitem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_addsubitem;
        private System.Windows.Forms.ComboBox ddl_item;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_subitem;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txt_amt;
        private System.Windows.Forms.Label lblamt;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView GVSubitem;
    }
}