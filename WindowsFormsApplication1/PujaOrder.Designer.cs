﻿namespace WindowsFormsApplication1
{
    partial class PujaOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet2 = new WindowsFormsApplication1.DataSet2();
            this.gv_pujari = new System.Windows.Forms.DataGridView();
            this.gporder = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_payment = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblmsg = new System.Windows.Forms.Label();
            this.lbl_amt = new System.Windows.Forms.Label();
            this.txt_amt = new System.Windows.Forms.TextBox();
            this.rb_udhar = new System.Windows.Forms.RadioButton();
            this.rb_cash = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Order = new System.Windows.Forms.Button();
            this.lbl_count = new System.Windows.Forms.Label();
            this.txt_count = new System.Windows.Forms.TextBox();
            this.lbl_subitem = new System.Windows.Forms.Label();
            this.lbl_item = new System.Windows.Forms.Label();
            this.ddl_subitem = new System.Windows.Forms.ComboBox();
            this.ddl_item = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataSet2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataTable1TableAdapter = new WindowsFormsApplication1.DataSet2TableAdapters.DataTable1TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_pujari)).BeginInit();
            this.gporder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.dataSet2;
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "DataSet2";
            this.dataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gv_pujari
            // 
            this.gv_pujari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_pujari.Location = new System.Drawing.Point(12, 57);
            this.gv_pujari.Name = "gv_pujari";
            this.gv_pujari.Size = new System.Drawing.Size(157, 457);
            this.gv_pujari.TabIndex = 0;
            this.gv_pujari.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gv_pujari_RowHeaderMouseDoubleClick);
            // 
            // gporder
            // 
            this.gporder.Controls.Add(this.button1);
            this.gporder.Controls.Add(this.txt_payment);
            this.gporder.Controls.Add(this.label4);
            this.gporder.Controls.Add(this.label3);
            this.gporder.Controls.Add(this.dateTimePicker1);
            this.gporder.Controls.Add(this.lblmsg);
            this.gporder.Controls.Add(this.lbl_amt);
            this.gporder.Controls.Add(this.txt_amt);
            this.gporder.Controls.Add(this.rb_udhar);
            this.gporder.Controls.Add(this.rb_cash);
            this.gporder.Controls.Add(this.label10);
            this.gporder.Controls.Add(this.label9);
            this.gporder.Controls.Add(this.label8);
            this.gporder.Controls.Add(this.label7);
            this.gporder.Controls.Add(this.label6);
            this.gporder.Controls.Add(this.label1);
            this.gporder.Controls.Add(this.button2);
            this.gporder.Controls.Add(this.Order);
            this.gporder.Controls.Add(this.lbl_count);
            this.gporder.Controls.Add(this.txt_count);
            this.gporder.Controls.Add(this.lbl_subitem);
            this.gporder.Controls.Add(this.lbl_item);
            this.gporder.Controls.Add(this.ddl_subitem);
            this.gporder.Controls.Add(this.ddl_item);
            this.gporder.Controls.Add(this.label2);
            this.gporder.Location = new System.Drawing.Point(580, 29);
            this.gporder.Name = "gporder";
            this.gporder.Size = new System.Drawing.Size(492, 485);
            this.gporder.TabIndex = 2;
            this.gporder.TabStop = false;
            this.gporder.Text = "Place Order";
            this.gporder.Enter += new System.EventHandler(this.gporder_Enter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(218, 451);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 28);
            this.button1.TabIndex = 25;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_payment
            // 
            this.txt_payment.Location = new System.Drawing.Point(60, 314);
            this.txt_payment.Name = "txt_payment";
            this.txt_payment.Size = new System.Drawing.Size(121, 20);
            this.txt_payment.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 317);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Payment";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(211, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(272, 314);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(132, 20);
            this.dateTimePicker1.TabIndex = 21;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Location = new System.Drawing.Point(60, 395);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 13);
            this.lblmsg.TabIndex = 20;
            // 
            // lbl_amt
            // 
            this.lbl_amt.AutoSize = true;
            this.lbl_amt.Location = new System.Drawing.Point(211, 254);
            this.lbl_amt.Name = "lbl_amt";
            this.lbl_amt.Size = new System.Drawing.Size(43, 13);
            this.lbl_amt.TabIndex = 17;
            this.lbl_amt.Text = "Amount";
            // 
            // txt_amt
            // 
            this.txt_amt.Location = new System.Drawing.Point(272, 247);
            this.txt_amt.Name = "txt_amt";
            this.txt_amt.ReadOnly = true;
            this.txt_amt.Size = new System.Drawing.Size(121, 20);
            this.txt_amt.TabIndex = 16;
            // 
            // rb_udhar
            // 
            this.rb_udhar.AutoSize = true;
            this.rb_udhar.Location = new System.Drawing.Point(214, 373);
            this.rb_udhar.Name = "rb_udhar";
            this.rb_udhar.Size = new System.Drawing.Size(54, 17);
            this.rb_udhar.TabIndex = 15;
            this.rb_udhar.TabStop = true;
            this.rb_udhar.Text = "Udhar";
            this.rb_udhar.UseVisualStyleBackColor = true;
            this.rb_udhar.CheckedChanged += new System.EventHandler(this.rb_udhar_CheckedChanged);
            // 
            // rb_cash
            // 
            this.rb_cash.AutoSize = true;
            this.rb_cash.Location = new System.Drawing.Point(150, 373);
            this.rb_cash.Name = "rb_cash";
            this.rb_cash.Size = new System.Drawing.Size(49, 17);
            this.rb_cash.TabIndex = 3;
            this.rb_cash.TabStop = true;
            this.rb_cash.Text = "Cash";
            this.rb_cash.UseVisualStyleBackColor = true;
            this.rb_cash.CheckedChanged += new System.EventHandler(this.rb_cash_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(310, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Address";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(177, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Mobile ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(310, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(177, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pujari Details ";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(312, 451);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 28);
            this.button2.TabIndex = 9;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Order
            // 
            this.Order.Location = new System.Drawing.Point(124, 451);
            this.Order.Name = "Order";
            this.Order.Size = new System.Drawing.Size(78, 28);
            this.Order.TabIndex = 8;
            this.Order.Text = "Order";
            this.Order.UseVisualStyleBackColor = true;
            this.Order.Click += new System.EventHandler(this.Order_Click);
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Location = new System.Drawing.Point(12, 254);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.Size = new System.Drawing.Size(46, 13);
            this.lbl_count.TabIndex = 7;
            this.lbl_count.Text = "Quantity";
            // 
            // txt_count
            // 
            this.txt_count.Location = new System.Drawing.Point(60, 247);
            this.txt_count.Name = "txt_count";
            this.txt_count.Size = new System.Drawing.Size(121, 20);
            this.txt_count.TabIndex = 6;
            this.txt_count.TextChanged += new System.EventHandler(this.txt_count_TextChanged);
            // 
            // lbl_subitem
            // 
            this.lbl_subitem.AutoSize = true;
            this.lbl_subitem.Location = new System.Drawing.Point(211, 180);
            this.lbl_subitem.Name = "lbl_subitem";
            this.lbl_subitem.Size = new System.Drawing.Size(45, 13);
            this.lbl_subitem.TabIndex = 5;
            this.lbl_subitem.Text = "Subitem";
            // 
            // lbl_item
            // 
            this.lbl_item.AutoSize = true;
            this.lbl_item.Location = new System.Drawing.Point(12, 180);
            this.lbl_item.Name = "lbl_item";
            this.lbl_item.Size = new System.Drawing.Size(27, 13);
            this.lbl_item.TabIndex = 4;
            this.lbl_item.Text = "Item";
            // 
            // ddl_subitem
            // 
            this.ddl_subitem.FormattingEnabled = true;
            this.ddl_subitem.Location = new System.Drawing.Point(272, 177);
            this.ddl_subitem.Name = "ddl_subitem";
            this.ddl_subitem.Size = new System.Drawing.Size(121, 21);
            this.ddl_subitem.TabIndex = 3;
            this.ddl_subitem.SelectedIndexChanged += new System.EventHandler(this.ddl_subitem_SelectedIndexChanged);
            // 
            // ddl_item
            // 
            this.ddl_item.FormattingEnabled = true;
            this.ddl_item.Items.AddRange(new object[] {
            "--Select--"});
            this.ddl_item.Location = new System.Drawing.Point(60, 177);
            this.ddl_item.Name = "ddl_item";
            this.ddl_item.Size = new System.Drawing.Size(121, 21);
            this.ddl_item.TabIndex = 2;
            this.ddl_item.SelectedIndexChanged += new System.EventHandler(this.ddl_item_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(59, 29);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Search";
            // 
            // dataSet2BindingSource
            // 
            this.dataSet2BindingSource.DataSource = this.dataSet2;
            this.dataSet2BindingSource.Position = 0;
            // 
            // reportViewer1
            // 
            reportDataSource2.Name = "DataSet1";
            reportDataSource2.Value = this.DataTable1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "WindowsFormsApplication1.Orderreport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(211, 57);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(328, 367);
            this.reportViewer1.TabIndex = 5;
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // PujaOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 747);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.gporder);
            this.Controls.Add(this.gv_pujari);
            this.Name = "PujaOrder";
            this.Text = "PujaOrder";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PujaOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_pujari)).EndInit();
            this.gporder.ResumeLayout(false);
            this.gporder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gv_pujari;
        private System.Windows.Forms.GroupBox gporder;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Order;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.TextBox txt_count;
        private System.Windows.Forms.Label lbl_subitem;
        private System.Windows.Forms.Label lbl_item;
        private System.Windows.Forms.ComboBox ddl_subitem;
        private System.Windows.Forms.ComboBox ddl_item;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton rb_udhar;
        private System.Windows.Forms.RadioButton rb_cash;
        private System.Windows.Forms.Label lbl_amt;
        private System.Windows.Forms.TextBox txt_amt;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txt_payment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource dataSet2BindingSource;
        private DataSet2 dataSet2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private DataSet2TableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
    }
}