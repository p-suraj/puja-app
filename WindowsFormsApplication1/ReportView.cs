﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace WindowsFormsApplication1
{
    public class ReportView
    {
        public string ItemName { get; set; }
        public string SubitemName { get; set; }
        public string TotalAmount { get; set; }
        public string Paytype { get; set; }
        public string Quantity { get; set; }
        public string Date { get; set; }
    }

    public class PaymentReport
    {
        public string TotalAmount { get; set; }
        public string Payment { get; set; }
        public string Date { get; set; }
    }

    public class PaymentDetailsReport
    {
        public string TotalAmount { get; set; }
        public string Payment { get; set; }
        public string Balance { get; set; }
    }
}
