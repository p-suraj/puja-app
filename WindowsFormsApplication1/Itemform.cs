﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

namespace WindowsFormsApplication1
{
    public partial class Itemform : Form
    {
        private Functions f;
        string mode = "I";
        public Itemform()
        {
            InitializeComponent();
            f = new Functions();
            Loadgrid();
        }

        public void Loadgrid()
        {
            string query = "select item_id as Id,item_name as ItemName from Item";
            f.Fillgrid(GVItem, query);
            //GVItem.SelectedRows[0]=InvalidUdtException]
            this.GVItem.Columns[0].Visible = false;
        }
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btn_Additem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtEmpty(txtitem.Text))
                {
                    MessageBox.Show("Please Enter Item Name", "Required");
                    txtitem.Focus();
                }
                else
                {
                    if (mode == "I")
                    {
                        f.conOpen();
                        var query = "Insert into Item(item_name) values('" + txtitem.Text + "')";
                        //f._command = new SqlCommand(query, f._connection);
                        //f._command.ExecuteNonQuery();
                        f.dml(query);
                        f.conClose();
                        MessageBox.Show("Record Inserted Successfully..");
                        txtitem.Text = "";
                        Loadgrid();
                    }

                    else if (mode == "U")
                    {
                        f.conOpen();
                        var updateitem = "update Item set item_name='" + txtitem.Text.Trim() + "' where item_id=" +
                                            GVItem.SelectedCells[0].Value;
                        f.dml(updateitem);
                        f.conClose();
                        MessageBox.Show("Record Updated Successfully....");
                        txtitem.Text = "";
                    }
                    Loadgrid();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void txtitem_Validating(object sender, CancelEventArgs e)
        {
            //    string error = null;
            //    if (txtitem.Text.Length == 0)
            //    {
            //        error = "Please Enter Item.";
            //        e.Cancel = true;
            //    }
            //    errorProvider1.SetError((Control)sender, error);
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("All Related records will be deleted, Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    //int i = GVSubitem.SelectedCells[0].RowIndex;
                    f.conOpen();
                    var deleteitem = "delete from Item where item_id=" + GVItem.SelectedCells[0].Value;
                    f.dml(deleteitem);
                    f.conClose();
                    MessageBox.Show("Record Deleted Successfully....");
                    Loadgrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Itemform_Load(object sender, EventArgs e)
        {

        }

        private void GVItem_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = GVItem.SelectedCells[0].RowIndex;
            txtitem.Text = GVItem.Rows[i].Cells[1].Value.ToString();
            mode = "U";
        }
        public bool txtEmpty(String txtbox)
        {
            string name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }

    }
}
