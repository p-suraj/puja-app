﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Addsubtem : Form
    {
        private Functions f;
        string mode = "I";
        public Addsubtem()
        {
            InitializeComponent();
            f = new Functions();
            f._command = new SqlCommand();
            FillCombo();
            mode = "I";
        }

        public void Loadgrid()
        {
            string query = "select s_id,item_name as ItemName,subitem_name,amount as Amount from subItem inner join Item ON subItem.item_id=Item.item_id ";
            f.Fillgrid(GVSubitem, query);
            GVSubitem.Columns[0].Visible = false;
        }
        private void FillCombo()
        {
            f._command.Connection = f._connection;
            f._command.CommandType = CommandType.Text;
            f._command.CommandText = "SELECT item_id,item_name FROM Item";
            var dt = new DataTable();
            var da = new SqlDataAdapter();
            da.SelectCommand = f._command;
            f.conOpen();
            da.Fill(dt);
            f.conClose();
            ddl_item.ValueMember = "item_id";
            ddl_item.DisplayMember = "item_name";
            ddl_item.DataSource = dt;

            ddl_item.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["item_name"] = "--Select--";
            dr["item_id"] = 0;

            dt.Rows.InsertAt(dr, 0);
            ddl_item.SelectedIndex = 0;

        }

        private void Clear()
        {
            txt_subitem.Text = "";
            txt_amt.Text = "";
        }
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Addsubtem_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'deviDataSet.Item' table. You can move, or remove it, as needed.
            //this.itemTableAdapter.Fill(this.deviDataSet.Item);
            Loadgrid();
        }

        private void btn_addsubitem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddl_item.SelectedIndex==0)
                {
                    MessageBox.Show("Please Select item name", "Requiered");
                    ddl_item.Focus();
                }
                else if (!txtEmpty(txt_subitem.Text))
                {
                    MessageBox.Show("Please Enter Subitem name", "Requiered");
                    txt_subitem.Focus();
                }
                else if (!txtEmpty(txt_amt.Text))
                {
                    MessageBox.Show("Please Enter Amount", "Requiered");
                    txt_amt.Focus();
                }
                else
                {
                    if (mode == "I")
                    {
                        f.conOpen();
                        var addsubitem = "Insert into subItem(item_id,subitem_name,amount) values(" + ddl_item.SelectedValue +
                                         ",'" + txt_subitem.Text + "'," + txt_amt.Text + ")";
                        f.dml(addsubitem);
                        f.conClose();
                        MessageBox.Show("Record Inserted Successfully....");
                        Clear();
                    }
                    else if (mode == "u")
                    {
                        f.conOpen();
                        var updatesubitem = "update subItem set item_id=" + ddl_item.SelectedValue + ",subitem_name='" +
                                            txt_subitem.Text.Trim() + "',amount=" + txt_amt.Text.Trim() + " where s_id=" +
                                            GVSubitem.SelectedCells[0].Value;
                        f.dml(updatesubitem);
                        f.conClose();
                        MessageBox.Show("Record Updated Successfully....");
                        Clear();
                    }

                    Loadgrid();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ddl_item_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string str = "select item_name from Item where item_name='" + ddl_item.Text.Trim().ToString() + "'";
            int _selectedIndex;
            _selectedIndex = ddl_item.SelectedIndex;
        }

        private void txt_amt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

       private void GVSubitem_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = GVSubitem.SelectedCells[0].RowIndex;
            txt_subitem.Text = GVSubitem.Rows[i].Cells[2].Value.ToString();
            txt_amt.Text = GVSubitem.Rows[i].Cells[3].Value.ToString();
            ddl_item.Text = GVSubitem.Rows[i].Cells[1].Value.ToString();
            mode = "u";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mode = "D";
            try
            {
                if (
                    MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //int i = GVSubitem.SelectedCells[0].RowIndex;
                    f.conOpen();
                    var deletesubitem = "delete from subItem where s_id=" + GVSubitem.SelectedCells[0].Value;
                    f.dml(deletesubitem);
                    f.conClose();
                    MessageBox.Show("Record Deleted Successfully....");
                    Loadgrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool txtEmpty(String txtbox)
        {
            string name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }
    }
}
