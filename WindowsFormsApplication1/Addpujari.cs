﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Addpujari : Form
    {
        private Functions f;
        private string mode = "I";
        public Addpujari()
        {
            InitializeComponent();
            f = new Functions();
            Loadgrid();
        }
        public void Loadgrid()
        {
            string query = "select pid, p_name As PujariName, mob1 as MobileNo, address as Address from Pujari";
            f.Fillgrid(GVPujari, query);
            this.GVPujari.Columns[0].Visible = false;
        }
        private void Clear()
        {
            txt_name.Text = "";
            txt_mob1.Text = "";
            txt_mob2.Text = "";
            txt_address.Text = "";
        }
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btn_addpujari_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtEmpty(txt_name.Text))
                {
                    MessageBox.Show("Please enter pujari's name", "Requiered");
                    txt_name.Focus();
                }
                else if (!txtEmpty(txt_mob1.Text))
                {
                    MessageBox.Show("Please enter Mobile No", "Requiered");
                    txt_mob1.Focus();
                }
                else if (!txtEmpty(txt_address.Text))
                {
                    MessageBox.Show("Please enter Address", "Requiered");
                    txt_address.Focus();
                }
                else
                {
                    if (mode == "I")
                    {
                        f.conOpen();
                        var addpujari = "Insert into Pujari(p_name,mob1,mob2,address) values('" + txt_name.Text + "','" +
                                        txt_mob1.Text + "','" + txt_mob2.Text + "','" + txt_address.Text + "')";
                        f.dml(addpujari);
                        f.conClose();
                        MessageBox.Show("Record Inserted Successfully....");
                        Clear();
                    }
                    else if (mode == "U")
                    {
                        f.conOpen();
                        var updatesubitem = "update Pujari set p_name='" + txt_name.Text.Trim() + "',mob1='" +
                                            txt_mob1.Text.Trim() + "',address='" + txt_address.Text.Trim() + "' where pid=" +
                                            GVPujari.SelectedCells[0].Value;
                        f.dml(updatesubitem);
                        f.conClose();
                        MessageBox.Show("Record Updated Successfully....");
                        Clear();
                    }
                    Loadgrid();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //int i = GVSubitem.SelectedCells[0].RowIndex;
                    f.conOpen();
                    var deletesubitem = "delete from Pujari where pid=" + GVPujari.SelectedCells[0].Value;
                    f.dml(deletesubitem);
                    f.conClose();
                    MessageBox.Show("Record Deleted Successfully....");
                    Loadgrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GVPujari_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = GVPujari.SelectedCells[0].RowIndex;
            txt_name.Text = GVPujari.Rows[i].Cells[1].Value.ToString();
            txt_mob1.Text = GVPujari.Rows[i].Cells[2].Value.ToString();
            txt_address.Text = GVPujari.Rows[i].Cells[3].Value.ToString();
            mode = "U";
        }
        public bool txtEmpty(String txtbox)
        {
            string name = txtbox;
            if (name.Length == 0)
                return false;
            else
                return true;
        }
    }
}
